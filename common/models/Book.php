<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use common\models\Folder;

class Book extends ActiveRecord {

	public static function tableName() {
		return '{{%book}}';
	}

	public function rules() {
		return [
			[['name'], 'string'],
			[['price', 'status'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Название',
			'desc' => 'Опиисание',
			'date' => 'Дата',
			'user' => 'Владелец',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function saveBook(){
		return $this->save();
	}

	public function getStatus(){
		switch ($this->status) {
			case 1: return 'Отсутствует';
			case 2: return 'Скоро будет';
			case 0: return 'На складе';
			default: return 'Ошибка';
		}
	}

	public static function allStatuses(){
		$statuses = [];
		$statuses[0] = 'На складе';
		$statuses[1] = 'Отсутствует';
		$statuses[2] = 'Скоро будет';
		return $statuses;
	}

}
