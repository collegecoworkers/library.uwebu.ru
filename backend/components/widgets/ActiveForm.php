<?php
namespace backend\components\widgets;

use Yii;

class ActiveForm extends \yii\widgets\ActiveForm
{
    public $fieldClass = 'backend\components\widgets\ActiveField';
}