<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\Book;
use common\models\Category;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'signup',
							'login',
							'error'
						],
						'allow' => true,
					],
					[
						'actions' => [
							'logout',
							'index',
							'add-category',
							'category',
							'down',
							'create',
							'update',
							'delete',
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Book::find()->orderBy(['date' => SORT_DESC]);
		return $this->render('index', [
			'model' => $model
		]);
	}

	public function actionCategory($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$category = Category::find()->where(['id' => $id])->one();
		$model = Book::find()->where(['cat_id' => $id]);
		return $this->render('category', [
			'category' => $category,
			'model' => $model
		]);
	}

	public function actionCreate() {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Book();

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Book']['name'];
			$model->price = Yii::$app->request->post()['Book']['price'];
			$model->status = Yii::$app->request->post()['Book']['status'];
			$model->cat_id = Yii::$app->request->post()['Book']['cat_id'];
			$model->desc = Yii::$app->request->post()['Book']['desc'];
			$model->date = date('Y-m-d');
			$model->save();
			return $this->redirect(['/site/index']);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Book::allStatuses(),
			'categories' => $this->getCats(),
		]);
	}

	public function actionUpdate($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Book::findIdentity($id);

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Book']['name'];
			$model->price = Yii::$app->request->post()['Book']['price'];
			$model->status = Yii::$app->request->post()['Book']['status'];
			$model->cat_id = Yii::$app->request->post()['Book']['cat_id'];
			$model->desc = Yii::$app->request->post()['Book']['desc'];
			$model->save();
			return $this->redirect(['/site/index']);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Book::allStatuses(),
			'categories' => $this->getCats(),
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Book::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionAddCategory() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Category();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Category']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-category', [
			'model' => $model,
		]);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function getCats() {
		$categories = [];
		$all_categories = Category::find()->all();

		for ($i=0; $i < count($all_categories); $i++) $categories[$all_categories[$i]->id] = $all_categories[$i]->title;

		return $categories;
	}

}
