<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $dataProvider backend\modules\contact\models\Contact */
/* @var $searchModel backend\modules\contact\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
		'pageSize' => 20,
	],
]);
?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
			<div class="contact-index">
				<div class="fa-br"></div>
				<br>
				<?php
					echo GridView::widget([
						'dataProvider' => $dataProvider,
						'layout' => "{items}\n{pager}",
						'columns' => [
							// ['class' => 'yii\grid\SerialColumn'],
							'id',
							[
								'label' => 'Ник',
								'attribute' => 'username',
								'format' => 'raw',
								'value' => function($dataProvider){
									return $dataProvider->username;
								},
							],
							'email:ntext',
							[
								'label' => 'Роль',
								'attribute' => 'status',
								'format' => 'raw',
								'value' => function($dataProvider){
									if($dataProvider->status == 0)
										return 'Пользователь';
									return 'Админ';
								},
							],
							[
								'class' => 'yii\grid\ActionColumn',
								'header'=>'Действия', 
								'headerOptions' => ['width' => '80'],
								'template' => '{update} {delete}{link}',
							],
						],
					]);
				?>
			</div>
		</div>
	</div>
</div>
