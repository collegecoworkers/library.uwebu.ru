<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Category;

$categories = [];
$all_categories = Category::find()->all();

for ($i=0; $i < count($all_categories); $i++) {
	$item = $all_categories[$i];
	$categories[] = [
		'label' => $item->title,
		'icon' => 'fa fa-archive',
		'url' => ["/site/category?id=$item->id"],
		'active' => ($this->context->route == 'site/category' and $_GET['id'] == $item->id)
	];
}
$categories[] = [
	'label' => 'Добавить Категорию',
	'icon' => 'fa fa-plus', 
	'url' => ['/site/add-category'],
	'active' => $this->context->route == 'site/add-category'
];

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?= Html::img('@web/img/user2-160x160.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
			</div>
			<div class="pull-left info">
				<p><?= \Yii::$app->user->identity->username ?></p>
				<a href=""><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php // var_dump($categories) ?>
		<?=
			Menu::widget(
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
						[
							'label' => 'Главная',
							'icon' => 'fa fa-home', 
							'url' => ['/'],
							'active' => $this->context->route == 'site/index'
						],
						[
							'label' => 'Категории',
							'icon' => 'fa fa-archive',
							'url' => '#',
							'items' => $categories
						],
					],
				]
			)
		?>
		
	</section>
	<!-- /.sidebar -->
</aside>
