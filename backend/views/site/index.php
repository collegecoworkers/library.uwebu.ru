<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Все книги');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

<div class="contact-index">
		<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/site/create') ?>
	<div class="fa-br"></div>
	<br>
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
		'columns' => [
			'name',
			[
				'label' => 'Цена',
				'attribute' => 'price',
				'value' => function($data){
					return $data->price . ' руб.';
				}
			],
			[
				'label' => 'Статус',
				'attribute' => 'status',
				'value' => function($data){
					return $data->getStatus();
				}
			],
			[
				'label' => 'Описание',
				'attribute' => 'desc',
				'value' => function($data){
					return substr($data->desc, 0, 100) . (strlen($data->desc) > 100 ? '...' : '');
				}
			],
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}',
			],
		],
	]);
	?>

</div>

		</div>
	</div>
</div>
