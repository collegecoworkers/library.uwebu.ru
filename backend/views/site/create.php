<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Новая запись');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"></div>
		<div class="panel-body">

			<div class="link-update">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'price')->textInput() ?>
				<?= $form->field($model, 'status')->dropDownList($statuses) ?>
				<?= $form->field($model, 'cat_id')->dropDownList($categories) ?>
				<?= $form->field($model, 'desc')->textArea() ?>

				<div class="form-group center">
					<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
