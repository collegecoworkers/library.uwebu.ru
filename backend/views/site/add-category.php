<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Новая категория');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"></div>
		<div class="panel-body">

			<div class="link-update">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'title')->textInput() ?>

				<div class="form-group center">
					<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
