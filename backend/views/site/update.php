<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Кандидаты');

$status = '';
?>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $model->title ?></div>
        <div class="panel-body">

<?php



$this->title = Yii::t('app', 'Update');

$this->params['breadcrumbs'][] = ['label' => 'Записи'];
$this->params['breadcrumbs'][] = ['label' => $model->title];
?>
<div class="link-update">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'title')->textInput(['placeholder' => '']) ?>
	<?= $form->field($model, 'desc')->textArea(['placeholder' => '']) ?>
	<?= $form->field($model, 'left_side')->dropDownList($sides, ['disabled' => 'disabled']) ?>
	<?= $form->field($model, 'right_side')->dropDownList($sides, ['disabled' => 'disabled']) ?>
	<?= $form->field($model, 'status')->dropDownList([
		'1' => 'Активен',
		'0' => 'Завершен',
	]) ?>


	<div class="form-group center">
		<?= ''// Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Yii::t('app', 'Update'), ['class' =>  'btn btn-success']) ?>
	</div>
	<?php ActiveForm::end(); ?>


</div>

        </div>
    </div>
</div>
