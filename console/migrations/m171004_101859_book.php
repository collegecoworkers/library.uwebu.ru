<?php

use yii\db\Migration;

class m171004_101859_book extends Migration
{
    public function safeUp()
    {
        $this->createTable('book', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'desc' => $this->text(),
            'price' => $this->integer(),
            'cat_id' => $this->integer(),
            'status' => $this->integer(),
            'date' => $this->date(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%book}}');
    }
}
