<?php

use yii\db\Migration;

class m171004_102550_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
